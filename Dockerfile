FROM gradle:6.8.3 AS builder
WORKDIR /src
COPY . .
RUN gradle installDist

FROM amazoncorretto:15.0.2-alpine AS service
WORKDIR /app
COPY --from=builder /src/build/install/flixbookings/ .

# Public details
WORKDIR /app/bin
EXPOSE 8080
CMD ["./flixbookings"]

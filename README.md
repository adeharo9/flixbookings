# [FLIXBookings](https://gitlab.com/adeharo9/flixbookings)

<img src="https://gitlab.com/adeharo9/flixbookings/-/raw/master/flixbus.png" alt="FLIXBookings" align="right"/>

![v1.1.1](https://img.shields.io/badge/version-v1.1.1-blue "v1.1.1")
![BSD 3-clause license](https://img.shields.io/badge/license-BSD%203--clause-green "BSD 3-clause license")

FLIXBookings - A FLIXBUS bookings API for the FLIXBUS Recruitment Challenge.

## Table of contents

1. [Usage](#usage)
   1. [Docker](#docker)
   2. [Local](#local)
2. [Documentation](#documentation)

## Usage

### Docker

The project comes with a [docker-compose](/docker-compose.yml) file. To use it, simply execute:

```shell
docker-compose up
```

The API will be readily available at `localhost:8080`.

### Local

In order to run this project locally, you'll need to have a MYSQL instance running. To configure it, copy the `.env.dist` file to `.env` and edit the needed variables accordingly.

After this, you can build and run the code with the provided `gradle/gradlew`.

## Documentation

The project comes with a SwaggerUI instance integrated into the application. To access it, once the application is running, simply access `localhost:8080/docs`.

package com.adeharo

import com.adeharo.routing.Routes.registerRoutes
import com.papsign.ktor.openapigen.OpenAPIGen
import io.github.cdimascio.dotenv.dotenv
import io.ktor.application.*
import io.ktor.features.*
import io.ktor.serialization.*
import org.flywaydb.core.Flyway
import org.jetbrains.exposed.sql.Database

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

fun Application.module() {
    val dotenv = dotenv {
        ignoreIfMissing = true
    }

    Flyway.configure().dataSource(
        dotenv["DB_URL"],
        dotenv["DB_USER"],
        dotenv["DB_PASSWORD"]
    ).load().migrate()

    Database.connect(
        dotenv["DB_URL"],
        user = dotenv["DB_USER"],
        password = dotenv["DB_PASSWORD"]
    )

    install(ContentNegotiation) {
        json()
    }

    install(OpenAPIGen) {
        serveSwaggerUi = true
        swaggerUiPath = "/swagger"
    }

    registerRoutes()
}

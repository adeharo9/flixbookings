package com.adeharo.application.exception

import io.ktor.http.*

class AlreadyExistsException(
    private val element: String = ""
): BaseException() {
    override val message: String
        get() = "$element already exists"
    override val status: HttpStatusCode
        get() =  HttpStatusCode.Forbidden
}
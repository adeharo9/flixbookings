package com.adeharo.application.exception

import io.ktor.http.*

class BadParameterException(
    private val parameter: String = ""
): BaseException() {
    override val message: String
        get() = "Parameter $parameter is malformed"
    override val status: HttpStatusCode
        get() =  HttpStatusCode.BadRequest
}
package com.adeharo.application.exception

import io.ktor.http.*

abstract class BaseException: Exception() {
    abstract override val message: String
    abstract val status: HttpStatusCode
}
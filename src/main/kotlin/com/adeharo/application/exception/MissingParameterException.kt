package com.adeharo.application.exception

import io.ktor.http.*

class MissingParameterException(
    private val parameter: String = ""
): BaseException() {
    override val message: String
        get() = "Missing parameter '$parameter'"
    override val status: HttpStatusCode
        get() =  HttpStatusCode.BadRequest
}
package com.adeharo.application.exception

import io.ktor.http.*

class NotEnoughAvailableSeatsException: BaseException() {
    override val message: String
        get() = "Available seats must be greater than or equal to 1"
    override val status: HttpStatusCode
        get() = HttpStatusCode.BadRequest
}
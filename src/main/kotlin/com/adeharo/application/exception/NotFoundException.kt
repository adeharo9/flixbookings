package com.adeharo.application.exception

import io.ktor.http.*

class NotFoundException(
    private val element: String = "Element"
): BaseException() {
    override val message: String
        get() = "$element not found"
    override val status: HttpStatusCode
        get() =  HttpStatusCode.NotFound
}
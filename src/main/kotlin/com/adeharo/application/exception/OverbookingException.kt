package com.adeharo.application.exception

import io.ktor.http.*

class OverbookingException : BaseException() {
    override val message: String
        get() = "Not enough seats are available on the selected trip"
    override val status: HttpStatusCode
        get() =  HttpStatusCode.Forbidden
}
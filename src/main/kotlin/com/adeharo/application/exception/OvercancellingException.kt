package com.adeharo.application.exception

import io.ktor.http.*

class OvercancellingException : BaseException() {
    override val message: String
        get() = "The number of seats to cancel cannot exceed those booked"
    override val status: HttpStatusCode
        get() =  HttpStatusCode.BadRequest
}
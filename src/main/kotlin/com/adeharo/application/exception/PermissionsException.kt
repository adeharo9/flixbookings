package com.adeharo.application.exception

import io.ktor.http.*

class PermissionsException : BaseException() {
    override val message: String
        get() = "You do not have permissions to perform this operation"
    override val status: HttpStatusCode
        get() =  HttpStatusCode.Unauthorized
}
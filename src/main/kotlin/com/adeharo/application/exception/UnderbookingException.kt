package com.adeharo.application.exception

import io.ktor.http.*

class UnderbookingException : BaseException() {
    override val message: String
        get() = "At least 1 seat must be booked"
    override val status: HttpStatusCode
        get() =  HttpStatusCode.BadRequest
}
package com.adeharo.application.exception

import io.ktor.http.*

class UndercancellingException : BaseException() {
    override val message: String
        get() = "The number of seats to cancel must be greater than or equal to 1"
    override val status: HttpStatusCode
        get() =  HttpStatusCode.BadRequest
}
package com.adeharo.application.service

import com.adeharo.application.exception.*
import com.adeharo.domain.model.entity.*
import com.adeharo.domain.model.exception.InvalidSlugException
import com.adeharo.domain.model.value.BookingCode
import com.adeharo.domain.model.value.Slug
import kotlinx.serialization.Serializable
import org.jetbrains.exposed.exceptions.ExposedSQLException
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import java.net.URLDecoder
import java.net.URLEncoder

object BookingService {
    @Serializable
    data class InsertOneRequest (
        val user_id: Int,
        val trip_id: Int,
        val seats: Int
    ) {
        val userId: Int
        get() = user_id
        val tripId: Int
        get() = trip_id
    }

    @Serializable
    data class BookingRequest (
        val email: String,
        val trip: String,
        val seats: Int
    ) {
        val tripSlug: String
        get() = trip
    }

    @Serializable
    data class CancellationRequest (
        val email: String,
        val code: String
    )

    fun getOneById(id: Int): Booking {
        return getOneBy(Bookings.id, id)
    }

    fun getOneBySlug(slug: String): Booking {
        try {
            /* First validate the given booking code */
            Slug.validate(slug)
        }
        catch (e: InvalidSlugException) {
            throw BadParameterException("slug [$slug]")
        }

        /* Then retrieve the requested booking */
        return getOneBy(Bookings.slug, slug)
    }

    fun getOneByCode(code: String): Booking {
        try {
            /* First validate the given booking code */
            BookingCode.validate(code)
        }
        catch (e: InvalidSlugException) {
            throw BadParameterException("code [$code]")
        }

        /* Then retrieve the requested booking */
        return getOneBy(Bookings.code, code)
    }

    fun getAll(filters: Map<String, List<String>>): List<Booking> {

        return transaction {
            val bookings = ArrayList<Booking>()

            Bookings
                .selectAll()
                .filter(filters)
                .forEach {
                    bookings.add(it.toBooking())
                }

            return@transaction bookings
        }
    }

    fun insertOne(request: InsertOneRequest): String {
        try {
            lateinit var slug: String
            transaction {
                val insert = Bookings
                    .insert {
                        it[userId] = request.userId
                        it[tripId] = request.tripId
                        it[seats] = request.seats
                    }
                slug = insert[Bookings.slug].toString()
            }

            return URLEncoder.encode(slug, "UTF-8")
        }
        catch (e: ExposedSQLException) {
            throw AlreadyExistsException("Booking")
        }
    }

    fun deleteOneById(id: Int) {
        /* First get the requested booking to check for its existence */
        getOneById(id)

        /* If it exists, delete it */
        deleteOneBy(Bookings.id, id)
    }

    fun deleteOneBySlug(slug: String) {
        /* First get the requested booking to check for its existence */
        getOneBySlug(slug)

        /* If it exists, delete it */
        deleteOneBy(Bookings.slug, slug)
    }

    fun deleteOneByCode(code: String) {
        /* First get the requested booking to check for its existence */
        getOneByCode(code)

        /* If it exists, delete it */
        deleteOneBy(Bookings.code, code)
    }

    fun book(request: BookingRequest): String {
        /* Check that both trip to book at and user exist and get them */
        val trip: Trip = TripService.getOneBySlug(request.tripSlug)
        val user: User = UserService.getOneByEmail(request.email)

        /* A booking must book at least 1 seat */
        if (request.seats <= 0) {
            throw UnderbookingException()
        }

        /* The requested seats to book cannot exceed the available
        * seats
        */
        if (trip.availableSeats < request.seats) {
            throw OverbookingException()
        }

        /* After all the checks, the transaction is ready to be
         * performed
         */

        /* First decrease the available seats of the trip. If something
         * goes wrong, it is preferable to have reserved seats with no
         * one to occupy them than to have a registered reservation with
         * seats not accounted on the trip available seats
         */
        transaction {
            /* Update the available seats of the trip */
            Trips
                .update({ Trips.slug eq request.tripSlug }) {
                    it[availableSeats] = trip.availableSeats - request.seats
                }
        }

        /* Insert the booking into the database */
        return insertOne(InsertOneRequest(user.id, trip.id, request.seats))
    }

    fun cancelBySlug(slug: String, seats: Int?) {
        /* Check that the booking to cancel from exists and take it
         */
        val booking: Booking = getOneBySlug(slug)

        /* Compute how many seats are requested to be cancelled. If they
         * have not been specified, assume the whole booking is to be
         * cancelled
         */
        val seatsToCancel = seats ?: booking.seats

        /* At least one seat must be cancelled */
        if (seatsToCancel <= 0) {
            throw UndercancellingException()
        }

        /* Seats to be cancelled cannot exceed booked seats */
        if (seatsToCancel > booking.seats) {
            throw OvercancellingException()
        }

        /* First, delete the requested seats from the booking. If
         * something goes wrong, it is preferable to have reserved seats
         * with no one to occupy them than to have a registered
         * reservation with seats not accounted on the trip available
         * seats
         *
         * If the number of seats to cancel corresponds to the number
         * of seats of the booking, cancel the whole booking by deleting
         * it
         */
        if (seatsToCancel >= booking.seats) {
            deleteOneBySlug(slug)
        }
        /* Otherwise, simply decrease the reservation seats by those
         * canceled
         */
        else {
            transaction {
                Bookings
                    .update({ Bookings.id eq booking.id }) {
                        with(SqlExpressionBuilder) {
                            it.update(Bookings.seats, Bookings.seats - seatsToCancel)
                        }
                    }
            }
        }

        /* Then, update the trip available seats accordingly */
        transaction {
            Trips
                .update({ Trips.id eq booking.trip.id }) {
                    with(SqlExpressionBuilder) {
                        it.update(availableSeats, availableSeats + seatsToCancel)
                    }
                }
        }
    }

    fun cancelByCode(request: CancellationRequest, seats: Int?) {
        /* Check that both booking to cancel from and user exist and get
         * them
         */
        val booking: Booking = getOneByCode(request.code)
        val user: User = UserService.getOneByEmail(request.email)

        /* Check that the booking was performed by the requesting user */
        if (user != booking.user) {
            throw PermissionsException()
        }

        cancelBySlug(booking.self.slug.toString(), seats)
    }

    private fun <T> getOneBy(column: Column<T>, value: T): Booking {
        try {
            return transaction {
                Bookings
                    .select { column eq value }
                    .single()
                    .toBooking()
            }
        }
        catch (e: NoSuchElementException) {
            throw NotFoundException("Booking ${value.toString()}")
        }
    }

    private fun <T> deleteOneBy(column: Column<T>, value: T) {
        transaction {
            Bookings
                .deleteWhere { column eq value }
        }
    }

    private fun Query.filter(filters: Map<String, List<String>>): Query {
        try {
            for ((k, v) in filters) {
                val value = URLDecoder.decode(v.first(), "UTF-8")
                when (k) {
                    "slug" -> this.andWhere {
                        Slug.validate(value)
                        Bookings.slug eq value
                    }
                    "code" -> this.andWhere {
                        BookingCode.validate(value)
                        Bookings.code eq value
                    }
                    "user" -> this.andWhere {
                        Slug.validate(value)
                        val user = UserService.getOneBySlug(value)
                        Bookings.userId eq user.id
                    }
                    "trip" -> this.andWhere {
                        Slug.validate(value)
                        val trip = TripService.getOneBySlug(value)
                        Bookings.tripId eq trip.id
                    }
                    "seats" -> this.andWhere {
                        Bookings.seats eq value.toInt()
                    }
                    else -> throw BadParameterException("$k [$value]")
                }
            }

            return this
        }
        catch (e: InvalidSlugException) {
            throw BadParameterException("slug")
        }
    }
}

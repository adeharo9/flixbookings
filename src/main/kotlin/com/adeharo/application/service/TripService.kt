package com.adeharo.application.service

import com.adeharo.application.exception.BadParameterException
import com.adeharo.application.exception.NotEnoughAvailableSeatsException
import com.adeharo.application.exception.NotFoundException
import com.adeharo.domain.model.entity.Trip
import com.adeharo.domain.model.entity.Trips
import com.adeharo.domain.model.entity.toTrip
import com.adeharo.domain.model.exception.InvalidSlugException
import com.adeharo.domain.model.value.DateTime
import com.adeharo.domain.model.value.Slug
import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import java.net.URLDecoder
import java.net.URLEncoder
import java.time.ZoneId
import java.time.ZonedDateTime

object TripService {
    @Serializable
    data class InsertOneRequest (
        val origin: String,
        val destination: String,
        @Serializable(with=DateTime.Serializer::class)
        val departure: ZonedDateTime,
        val available_seats: Int
    ) {
        val availableSeats: Int
        get() = available_seats
    }

    fun getOneById(id: Int): Trip {
        return getOneBy(Trips.id, id)
    }

    fun getOneBySlug(slug: String): Trip {
        try {
            Slug.validate(slug)
        }
        catch (e: InvalidSlugException) {
            throw BadParameterException("slug [$slug]")
        }

        return getOneBy(Trips.slug, slug)
    }

    fun getAll(filters: Map<String, List<String>>): List<Trip> {
        return transaction {
            val trips = ArrayList<Trip>()

            Trips
                .selectAll()
                .filter(filters)
                .forEach {
                    trips.add(it.toTrip())
                }

            return@transaction trips
        }
    }

    fun insertOne(request: InsertOneRequest): String {
        lateinit var slug: String

        if (request.availableSeats <= 0) {
            throw NotEnoughAvailableSeatsException()
        }

        transaction {
            val insert = Trips
                .insert {
                    it[origin] = request.origin
                    it[destination] = request.destination
                    it[departure] = request.departure
                        .withZoneSameInstant(ZoneId.of("UTC"))
                        .toLocalDateTime()
                    it[availableSeats] = request.availableSeats
                }
            slug = insert[Trips.slug].toString()
        }

        return URLEncoder.encode(slug, "UTF-8")
    }

    fun deleteOneById(id: Int) {
        getOneById(id)
        deleteOneBy(Trips.id, id)
    }

    fun deleteOneBySlug(slug: String) {
        getOneBySlug(slug)
        deleteOneBy(Trips.slug, slug)
    }

    private fun <T> getOneBy(column: Column<T>, value: T): Trip {
        try {
            return transaction {
                Trips
                    .select { column eq value }
                    .single()
                    .toTrip()
            }
        }
        catch (e: NoSuchElementException) {
            throw NotFoundException("Trip ${value.toString()}")
        }
    }

    private fun <T> deleteOneBy(column: Column<T>, value: T) {
        transaction {
            Trips
                .deleteWhere { column eq value }
        }
    }

    private fun Query.filter(filters: Map<String, List<String>>): Query {
        try {
            for((k, v) in filters) {
                val value = URLDecoder.decode(v.first(), "UTF-8")
                when(k) {
                    "slug" -> this.andWhere {
                        Slug.validate(value)
                        Trips.slug eq value
                    }
                    "origin" -> this.andWhere{ Trips.origin eq value }
                    "destination" -> this.andWhere{ Trips.destination eq value }
                    "departure" -> this.andWhere{
                        Trips.departure eq ZonedDateTime
                            .parse(value)
                            .withZoneSameInstant(ZoneId.of("UTC"))
                            .toLocalDateTime()
                    }
                    "available_seats" -> this.andWhere {
                        Trips.availableSeats eq value.toInt()
                    }
                    else -> throw BadParameterException("$k [$value]")
                }
            }

            return this
        }
        catch (e: InvalidSlugException) {
            throw BadParameterException("slug")
        }
    }
}
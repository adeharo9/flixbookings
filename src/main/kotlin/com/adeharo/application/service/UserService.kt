package com.adeharo.application.service

import com.adeharo.application.exception.AlreadyExistsException
import com.adeharo.application.exception.BadParameterException
import com.adeharo.application.exception.NotFoundException
import com.adeharo.domain.model.entity.User
import com.adeharo.domain.model.entity.Users
import com.adeharo.domain.model.entity.toUser
import com.adeharo.domain.model.exception.InvalidEmailException
import com.adeharo.domain.model.exception.InvalidSlugException
import com.adeharo.domain.model.value.Email
import com.adeharo.domain.model.value.Slug
import kotlinx.serialization.Serializable
import org.jetbrains.exposed.exceptions.ExposedSQLException
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import java.net.URLDecoder
import java.net.URLEncoder

object UserService {
    @Serializable
    data class InsertOneRequest (
        val email: String,
        val password: String? = null
    )

    fun getOneById(id: Int): User {
        return getOneBy(Users.id, id)
    }

    fun getOneBySlug(slug: String): User {
        try {
            Slug.validate(slug)
        }
        catch (e: InvalidSlugException) {
            throw BadParameterException("slug [$slug]")
        }

        return getOneBy(Users.slug, slug)
    }

    fun getOneByEmail(email: String): User {
        try {
            Email.validate(email)
        }
        catch (e: InvalidEmailException) {
            throw BadParameterException("email [$email]")
        }

        return getOneBy(Users.email, email)
    }

    fun getAll(filters: Map<String, List<String>>): List<User> {
        return transaction {
            val users = ArrayList<User>()

            Users
                .selectAll()
                .filter(filters)
                .forEach {
                    users.add(it.toUser())
                }

            return@transaction users
        }
    }

    fun insertOne(request: InsertOneRequest): String {
        try {
            lateinit var slug: String
            Email.validate(request.email)
            transaction {
                val insert = Users
                    .insert {
                        it[email] = request.email
                        it[password] = request.password
                        it[registered] = request.password != null
                    }
                slug = insert[Users.slug].toString()
            }

            return URLEncoder.encode(slug, "UTF-8")
        }
        catch (e: InvalidEmailException) {
            throw BadParameterException("email [${request.email}]")
        }
        catch (e: ExposedSQLException) {
            throw AlreadyExistsException("User ${request.email}")
        }
    }

    fun deleteOneById(id: Int) {
        getOneById(id)
        deleteOneBy(Users.id, id)
    }

    fun deleteOneBySlug(slug: String) {
        getOneBySlug(slug)
        deleteOneBy(Users.slug, slug)
    }

    fun deleteOneByEmail(email: String) {
        getOneByEmail(email)
        deleteOneBy(Users.email, email)
    }

    private fun <T> getOneBy(column: Column<T>, value: T): User {
        try {
            return transaction {
                Users
                    .select { column eq value }
                    .single()
                    .toUser()
            }
        }
        catch (e: NoSuchElementException) {
            throw NotFoundException("User ${value.toString()}")
        }
    }

    private fun <T> deleteOneBy(column: Column<T>, value: T) {
        transaction {
            Users
                .deleteWhere { column eq value }
        }
    }

    private fun Query.filter(filters: Map<String, List<String>>): Query {
        try {
            for((k, v) in filters) {
                val value = URLDecoder.decode(v.first(), "UTF-8")
                when(k) {
                    "slug" -> this.andWhere {
                        Slug.validate(value)
                        Users.slug eq value
                    }
                    "email" -> this.andWhere{
                        Email.validate(value)
                        Users.email eq value
                    }
                    "registered" -> this.andWhere{
                        Users.registered eq value.toBoolean()
                    }
                    else -> throw BadParameterException("$k [$value]")
                }
            }

            return this
        }
        catch (e: InvalidSlugException) {
            throw BadParameterException("slug")
        }
        catch (e: InvalidEmailException) {
            throw BadParameterException("email")
        }
    }
}
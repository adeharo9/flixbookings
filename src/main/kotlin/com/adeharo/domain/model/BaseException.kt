package com.adeharo.domain.model

abstract class BaseException: Exception() {
    abstract override val message: String
}
package com.adeharo.domain.model.entity

import com.adeharo.application.service.TripService
import com.adeharo.application.service.UserService
import com.adeharo.domain.model.value.BookingCode
import com.adeharo.domain.model.value.DateTime
import com.adeharo.domain.model.value.Slug
import com.adeharo.domain.model.value.URI
import com.adeharo.routing.Routes
import kotlinx.serialization.*
import kotlinx.serialization.builtins.nullable
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.descriptors.buildClassSerialDescriptor
import kotlinx.serialization.descriptors.element
import kotlinx.serialization.encoding.CompositeDecoder.Companion.DECODE_DONE
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.encoding.decodeStructure
import kotlinx.serialization.encoding.encodeStructure
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.`java-time`.CurrentDateTime
import org.jetbrains.exposed.sql.`java-time`.datetime
import java.time.ZonedDateTime

object Bookings: Table("bookings") {
    val id = integer("id").autoIncrement()
    val slug = varchar("slug", 16).clientDefault { Slug.random.toString() }.uniqueIndex()
    val code = varchar("code", 6).clientDefault { BookingCode.random.toString() }.uniqueIndex()
    val userId = integer("user_id").references(Users.id)
    val tripId = integer("trip_id").references(Trips.id)
    val seats = integer("seats")
    val createdAt = datetime("created_at").defaultExpression(CurrentDateTime())
    val updatedAt = datetime("updated_at").defaultExpression(CurrentDateTime())
    val deletedAt = datetime("deleted_at").nullable()

    override val primaryKey = PrimaryKey(id)
}

@Serializable(with = Booking.Serializer::class)
data class Booking (
    @Transient
    val id: Int = -1,
    val self: URI,
    val code: String,
    val user: User,
    val trip: Trip,
    val seats: Int,
    @Serializable(with=DateTime.Serializer::class)
    val createdAt: ZonedDateTime,
    @Serializable(with=DateTime.Serializer::class)
    val updatedAt: ZonedDateTime,
    @Serializable(with=DateTime.Serializer::class)
    val deletedAt: ZonedDateTime?
) {
    override fun equals(other: Any?): Boolean {
        return other is Booking
                && if (this.id >= 0 && other.id >= 0) {
            this.id == other.id
        } else {
            this.self == other.self
        }
    }

    override fun hashCode(): Int {
        return self.hashCode()
    }

    @OptIn(ExperimentalSerializationApi::class)
    @kotlinx.serialization.Serializer(forClass = Booking::class)
    object Serializer: KSerializer<Booking> {
        override val descriptor: SerialDescriptor = buildClassSerialDescriptor("com.adeharo.domain.model.entity.Booking") {
            element<String>("self")
            element<String>("code")
            element<String>("user")
            element<String>("trip")
            element<Int>("seats")
            element<String>("createdAt")
            element<String>("updatedAt")
            element<String?>("deletedAt", isOptional = true)
        }

        override fun serialize(encoder: Encoder, value: Booking) {
            encoder.encodeStructure(descriptor) {
                encodeSerializableElement(descriptor, 0, URI.Serializer, value.self)
                encodeStringElement(descriptor, 1, value.code)
                encodeSerializableElement(descriptor, 2, URI.Serializer, value.user.self)
                encodeSerializableElement(descriptor, 3, URI.Serializer, value.trip.self)
                encodeIntElement(descriptor,4, value.seats)
                encodeSerializableElement(descriptor, 5, DateTime.Serializer, value.createdAt)
                encodeSerializableElement(descriptor, 6, DateTime.Serializer, value.updatedAt)
                encodeNullableSerializableElement(descriptor, 7, DateTime.Serializer, value.deletedAt)
            }
        }

        override fun deserialize(decoder: Decoder): Booking {
            return decoder.decodeStructure(descriptor) {
                var self: URI? = null
                var code: String? = null
                var user: User? = null
                var trip: Trip? = null
                var seats: Int? = null
                var createdAt: ZonedDateTime? = null
                var updatedAt: ZonedDateTime? = null
                var deletedAt: ZonedDateTime? = null

                loop@ while (true) {
                    when (val index = decodeElementIndex(descriptor)) {
                        DECODE_DONE -> break@loop

                        0 -> self = decodeSerializableElement(descriptor, 0, URI.Serializer)
                        1 -> code = decodeStringElement(descriptor, 1)
                        2 -> {
                            val uri = decodeSerializableElement(descriptor, 2, URI.Serializer)
                            user = UserService.getOneBySlug(uri.slug.toString())
                        }
                        3 -> {
                            val uri = decodeSerializableElement(descriptor, 3, URI.Serializer)
                            trip = TripService.getOneBySlug(uri.slug.toString())
                        }
                        4 -> seats = decodeIntElement(descriptor, 4)
                        5 -> createdAt = decodeSerializableElement(descriptor, 5, DateTime.Serializer)
                        6 -> updatedAt = decodeSerializableElement(descriptor, 6, DateTime.Serializer)
                        7 -> deletedAt = decodeNullableSerializableElement(descriptor, 7, DateTime.Serializer.nullable)

                        else -> throw SerializationException("Unexpected index $index")
                    }
                }

                Booking(
                    self = requireNotNull(self),
                    code = requireNotNull(code),
                    user = requireNotNull(user),
                    trip = requireNotNull(trip),
                    seats = requireNotNull(seats),
                    createdAt = requireNotNull(createdAt),
                    updatedAt = requireNotNull(updatedAt),
                    deletedAt = deletedAt
                )
            }
        }
    }
}

fun ResultRow.toBooking(): Booking {
    val user = UserService.getOneById(this[Bookings.userId])
    val trip = TripService.getOneById(this[Bookings.tripId])

    return Booking(
        id = this[Bookings.id],
        self = URI("${Routes.booking}/${this[Bookings.slug]}"),
        code = this[Bookings.code],
        user = user,
        trip = trip,
        seats = this[Bookings.seats],
        createdAt = DateTime.applyUTC(this[Bookings.createdAt]),
        updatedAt = DateTime.applyUTC(this[Bookings.updatedAt]),
        deletedAt = DateTime.applyUTCOrNull(this[Bookings.deletedAt])
    )
}

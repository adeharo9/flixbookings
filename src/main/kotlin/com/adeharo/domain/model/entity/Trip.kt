package com.adeharo.domain.model.entity

import com.adeharo.domain.model.value.DateTime
import com.adeharo.domain.model.value.Slug
import com.adeharo.domain.model.value.URI
import com.adeharo.routing.Routes
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.`java-time`.CurrentDateTime
import org.jetbrains.exposed.sql.`java-time`.datetime
import java.time.ZonedDateTime

object Trips: Table("trips") {
    val id = integer("id").autoIncrement()
    val slug = varchar("slug", 16).clientDefault { Slug.random.toString() }.uniqueIndex()
    val origin = text("origin")
    val destination = text("destination")
    val departure = datetime("departure")
    val availableSeats = integer("available_seats")
    val createdAt = datetime("created_at").defaultExpression(CurrentDateTime())
    val updatedAt = datetime("updated_at").defaultExpression(CurrentDateTime())
    val deletedAt = datetime("deleted_at").nullable()

    override val primaryKey = PrimaryKey(id)
}

@Serializable
data class Trip(
    @Transient
    val id: Int = -1,
    val self: URI,
    val origin: String,
    val destination: String,
    @Serializable(with=DateTime.Serializer::class)
    val departure: ZonedDateTime,
    val availableSeats: Int,
    @Serializable(with=DateTime.Serializer::class)
    val createdAt: ZonedDateTime,
    @Serializable(with=DateTime.Serializer::class)
    val updatedAt: ZonedDateTime,
    @Serializable(with=DateTime.Serializer::class)
    val deletedAt: ZonedDateTime?
) {
    override fun equals(other: Any?): Boolean {
        return other is Trip
                && if (this.id >= 0 && other.id >= 0) {
            this.id == other.id
        } else {
            this.self == other.self
        }
    }

    override fun hashCode(): Int {
        return self.hashCode()
    }
}

fun ResultRow.toTrip(): Trip {
    return Trip(
        id = this[Trips.id],
        self = URI("${Routes.trip}/${this[Trips.slug]}"),
        origin = this[Trips.origin],
        destination = this[Trips.destination],
        departure = DateTime.applyUTC(this[Trips.departure]),
        availableSeats = this[Trips.availableSeats],
        createdAt = DateTime.applyUTC(this[Trips.createdAt]),
        updatedAt = DateTime.applyUTC(this[Trips.updatedAt]),
        deletedAt = DateTime.applyUTCOrNull(this[Trips.deletedAt])
    )
}

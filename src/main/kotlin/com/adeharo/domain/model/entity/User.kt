package com.adeharo.domain.model.entity

import com.adeharo.domain.model.value.DateTime
import com.adeharo.domain.model.value.Slug
import com.adeharo.domain.model.value.URI
import com.adeharo.routing.Routes
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.`java-time`.CurrentDateTime
import org.jetbrains.exposed.sql.`java-time`.datetime
import java.time.ZonedDateTime

object Users: Table("users") {
    val id = integer("id").autoIncrement()
    val slug = varchar("slug", 16).clientDefault { Slug.random.toString() }.uniqueIndex()
    val email = varchar("email", 255).uniqueIndex()
    val password = text("password").nullable()
    val registered = bool("registered").default(true)
    val createdAt = datetime("created_at").defaultExpression(CurrentDateTime())
    val updatedAt = datetime("updated_at").defaultExpression(CurrentDateTime())
    val deletedAt = datetime("deleted_at").nullable()

    override val primaryKey = PrimaryKey(id)
}

@Serializable
data class User(
    @Transient
    val id: Int = -1,
    val self: URI,
    val email: String,
    val password: String?,
    val registered: Boolean,
    @Serializable(with=DateTime.Serializer::class)
    val createdAt: ZonedDateTime,
    @Serializable(with=DateTime.Serializer::class)
    val updatedAt: ZonedDateTime,
    @Serializable(with=DateTime.Serializer::class)
    val deletedAt: ZonedDateTime?
) {
    override fun equals(other: Any?): Boolean {
        return other is User
                && if (this.id >= 0 && other.id >= 0) {
            this.id == other.id
        } else {
            this.self == other.self
        }
    }

    override fun hashCode(): Int {
        return self.hashCode()
    }
}

fun ResultRow.toUser(): User {
    return User(
        id = this[Users.id],
        self = URI("${Routes.user}/${this[Users.slug]}"),
        email = this[Users.email],
        password = this[Users.password],
        registered = this[Users.registered],
        createdAt = DateTime.applyUTC(this[Users.createdAt]),
        updatedAt = DateTime.applyUTC(this[Users.updatedAt]),
        deletedAt = DateTime.applyUTCOrNull(this[Users.deletedAt])
    )
}

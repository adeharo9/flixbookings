package com.adeharo.domain.model.exception

import com.adeharo.domain.model.BaseException

class InvalidBookingCodeException: BaseException() {
    override val message: String
        get() = "Booking code is invalid"
}
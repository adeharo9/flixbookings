package com.adeharo.domain.model.exception

import com.adeharo.domain.model.BaseException

class InvalidEmailException: BaseException() {
    override val message: String
        get() = "Email is invalid"
}
package com.adeharo.domain.model.exception

import com.adeharo.domain.model.BaseException

class InvalidSlugException: BaseException() {
    override val message: String
        get() = "Slug is invalid"
}
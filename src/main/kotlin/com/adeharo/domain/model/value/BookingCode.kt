package com.adeharo.domain.model.value

import com.adeharo.domain.model.exception.InvalidBookingCodeException
import java.util.*
import kotlin.streams.asSequence

class BookingCode() {
    private var code: String
    val length: Int = 6

    init {
        val source = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        code = Random().ints(length.toLong(), 0, source.length)
            .asSequence()
            .map(source::get)
            .joinToString("")
    }

    private constructor(code: String) : this() {
        validate(code)
        this.code = code
    }

    override fun equals(other: Any?): Boolean {
        return other is BookingCode
                && other.code == this.code
    }

    override fun hashCode(): Int {
        return code.hashCode()
    }

    override fun toString(): String {
        return code
    }

    companion object {
        val random: BookingCode
        get() = BookingCode()

        private val validatorRegex = """[A-Z0-9]{6}""".toRegex()

        fun fromString(code: String): BookingCode {
            return BookingCode(code)
        }

        fun isValid(code: String): Boolean {
            return validatorRegex.matches(code)
        }

        fun validate(code: String) {
            if (!isValid(code)) {
                throw InvalidBookingCodeException()
            }
        }
    }
}

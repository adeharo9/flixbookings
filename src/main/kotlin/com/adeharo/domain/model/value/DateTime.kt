@file:Suppress("UnusedImport", "UnusedImport")

package com.adeharo.domain.model.value

import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.KSerializer
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZonedDateTime

class DateTime {
    companion object {
        fun applyUTC(localDateTime: LocalDateTime): ZonedDateTime {
            return ZonedDateTime.of(localDateTime, ZoneId.of("Z"))
        }

        fun applyUTCOrNull(localDateTime: LocalDateTime?): ZonedDateTime? {
            return if(localDateTime != null) ZonedDateTime.of(localDateTime, ZoneId.of("Z")) else null
        }
    }

    @OptIn(ExperimentalSerializationApi::class)
    @kotlinx.serialization.Serializer(forClass = ZonedDateTime::class)
    object Serializer: KSerializer<ZonedDateTime> {
        override fun serialize(encoder: Encoder, value: ZonedDateTime) {
            encoder.encodeString(value.toString())
        }

        override fun deserialize(decoder: Decoder): ZonedDateTime {
            return ZonedDateTime.parse(decoder.decodeString())
        }
    }
}
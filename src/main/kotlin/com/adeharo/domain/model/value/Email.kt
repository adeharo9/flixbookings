package com.adeharo.domain.model.value

import com.adeharo.domain.model.exception.InvalidEmailException

class Email private constructor(private val email: String) {
    init {
        validate(email)
    }

    override fun equals(other: Any?): Boolean {
        return other is Email
                && other.email == this.email
    }

    override fun hashCode(): Int {
        return email.hashCode()
    }

    override fun toString(): String {
        return email
    }

    companion object {
        private val validatorRegex = """[a-zA-Z].*@.+\..+""".toRegex()
        fun fromString(email: String): Email {
            return Email(email)
        }

        fun isValid(email: String): Boolean {
            return validatorRegex.matches(email)
        }

        fun validate(email: String) {
            if (!isValid(email)) {
                throw InvalidEmailException()
            }
        }
    }
}
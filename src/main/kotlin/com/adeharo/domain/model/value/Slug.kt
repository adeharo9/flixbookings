package com.adeharo.domain.model.value

import com.adeharo.domain.model.exception.InvalidSlugException
import java.util.*
import kotlin.streams.asSequence

class Slug() {
    val length: Int = 16
    private var str: String

    init {
        val source = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        str = Random().ints(length.toLong(), 0, source.length)
            .asSequence()
            .map(source::get)
            .joinToString("")
    }

    private constructor(slug: String) : this() {
        validate(slug)
        this.str = slug
    }

    override fun equals(other: Any?): Boolean {
        return other is Slug
                && other.str == this.str
    }

    override fun hashCode(): Int {
        return str.hashCode()
    }

    override fun toString(): String {
        return str
    }

    companion object {
        val random: Slug
        get() = Slug()

        private val validatorRegex = """[a-zA-Z0-9]{16}""".toRegex()

        fun fromString(slug: String): Slug {
            return Slug(slug)
        }

        fun fromURI(uri: String): Slug {
            /** Delete the trailing slash (slashless URI) */
            val slURI = if (uri.endsWith("/")) uri.dropLast(1) else uri
            return Slug(slURI.split("/").last())
        }

        fun isValid(slug: String): Boolean {
            return validatorRegex.matches(slug)
        }

        fun validate(slug: String) {
            if (!isValid(slug)) {
                throw InvalidSlugException()
            }
        }
    }
}

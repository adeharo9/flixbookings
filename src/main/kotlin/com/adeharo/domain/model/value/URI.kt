package com.adeharo.domain.model.value

import com.adeharo.domain.model.exception.InvalidSlugException
import com.adeharo.domain.model.exception.InvalidURIException
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

@Serializable(with = URI.Serializer::class)
class URI(private val uri: String) {
    val slug: Slug = Slug.fromURI(uri)

    override fun equals(other: Any?): Boolean {
        return other is URI
                && this.uri == other.uri
    }

    override fun hashCode(): Int {
        return uri.hashCode()
    }

    override fun toString(): String {
        return uri
    }

    companion object {
        fun fromString(uri: String): URI {
            return URI(uri)
        }

        fun isValid(uri: String): Boolean {
            var valid = true

            try {
                Slug.fromURI(uri)
            }
            catch (e: InvalidSlugException) {
                valid = false
            }

            return valid
        }

        fun validate(uri: String) {
            if (!isValid(uri)) {
                throw InvalidURIException()
            }
        }
    }

    @OptIn(ExperimentalSerializationApi::class)
    @kotlinx.serialization.Serializer(forClass = URI::class)
    object Serializer: KSerializer<URI> {
        override fun serialize(encoder: Encoder, value: URI) {
            encoder.encodeString(value.toString())
        }

        override fun deserialize(decoder: Decoder): URI {
            return URI(decoder.decodeString())
        }
    }
}
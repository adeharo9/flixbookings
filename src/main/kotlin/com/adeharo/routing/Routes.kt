package com.adeharo.routing

import com.adeharo.routing.service.bookingRouting
import com.adeharo.routing.service.docsRouting
import com.adeharo.routing.service.tripRouting
import com.adeharo.routing.service.userRouting
import io.ktor.application.*
import io.ktor.routing.*

object Routes {
    val docs: String = "/docs"
    val booking: String = "/booking"
    val trip: String = "/trip"
    val user: String = "/user"

    fun Application.registerRoutes() {
        routing {
            docsRouting()
            bookingRouting()
            tripRouting()
            userRouting()
        }
    }
}
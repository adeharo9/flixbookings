package com.adeharo.routing.response

import io.ktor.http.*
import kotlinx.serialization.Serializable

@Serializable
abstract class BaseResponse {
    abstract val status: Status

    @Serializable
    class Status(
        val code: Int,
        val description: String = ""
    ) {
        constructor(httpStatusCode: HttpStatusCode) : this(
            httpStatusCode.value, httpStatusCode.description
        )
    }
}


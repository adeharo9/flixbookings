package com.adeharo.routing.response

import io.ktor.http.*
import kotlinx.serialization.Serializable

@Serializable
class DataResponse<T>(
    val data: T,
    override val status: Status = Status(HttpStatusCode.OK)
): BaseResponse() {
    constructor(data: T, httpStatusCode: HttpStatusCode): this(
        data,
        Status(httpStatusCode)
    )
}
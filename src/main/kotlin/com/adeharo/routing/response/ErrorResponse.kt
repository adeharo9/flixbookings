package com.adeharo.routing.response

import com.adeharo.application.exception.BaseException
import io.ktor.http.*
import kotlinx.serialization.Serializable

@Serializable
class ErrorResponse(
    val error: String,
    override val status: Status
): BaseResponse() {
    constructor(error: String, httpStatusCode: HttpStatusCode): this(
        error,
        Status(httpStatusCode)
    )

    constructor(e: BaseException): this(e.message, e.status)
}
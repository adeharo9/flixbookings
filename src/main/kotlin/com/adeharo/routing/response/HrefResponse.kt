package com.adeharo.routing.response

import io.ktor.http.*
import kotlinx.serialization.Serializable

@Serializable
class HrefResponse(
    val href: String,
    val message: String,
    override val status: Status = Status(HttpStatusCode.OK)
): BaseResponse() {
    constructor(href: String, message: String, httpStatusCode: HttpStatusCode): this(
        href,
        message,
        Status(httpStatusCode)
    )
}
package com.adeharo.routing.response

import io.ktor.http.*
import kotlinx.serialization.Serializable

@Serializable
class MessageResponse(
    val message: String,
    override val status: Status = Status(HttpStatusCode.OK)
): BaseResponse() {
    constructor(message: String, httpStatusCode: HttpStatusCode): this(
        message,
        Status(httpStatusCode)
    )
}
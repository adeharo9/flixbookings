package com.adeharo.routing.service

import com.adeharo.application.exception.BadParameterException
import com.adeharo.application.exception.BaseException
import com.adeharo.application.exception.MissingParameterException
import com.adeharo.application.service.BookingService
import com.adeharo.routing.Routes
import com.adeharo.routing.response.DataResponse
import com.adeharo.routing.response.ErrorResponse
import com.adeharo.routing.response.HrefResponse
import com.adeharo.routing.response.MessageResponse
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.util.*

fun Route.bookingRouting() {
    route(Routes.booking) {
        get ("{slug}") {
            try {
                val slug = call.parameters["slug"] ?:
                    throw MissingParameterException("slug")
                val booking = BookingService.getOneBySlug(slug)
                call.respond(DataResponse(booking))
            }
            catch (e: BaseException) {
                call.respond(e.status, ErrorResponse(e))
            }
            catch (e: Throwable) {
                call.respond(
                    HttpStatusCode.InternalServerError,
                    ErrorResponse(
                        e.message ?: "Internal server error",
                        HttpStatusCode.InternalServerError
                    )
                )
            }
        }
        get {
            try {
                val filters = call.request.queryParameters.toMap()
                val bookings = BookingService.getAll(filters)
                call.respond(DataResponse(bookings))
            }
            catch (e: BaseException) {
                call.respond(e.status, ErrorResponse(e))
            }
            catch (e: Throwable) {
                call.respond(
                    HttpStatusCode.InternalServerError,
                    ErrorResponse(
                        e.message ?: "Internal server error",
                        HttpStatusCode.InternalServerError
                    )
                )
            }
        }
        post {
            try {
                val request = call.receive<BookingService.BookingRequest>()
                val slug = BookingService.book(request)
                call.respond(HrefResponse(
                    "${Routes.booking}/$slug",
                    "Booking successful"
                ))
            }
            catch (e: BaseException) {
                call.respond(e.status, ErrorResponse(e))
            }
            catch (e: Throwable) {
                call.respond(
                    HttpStatusCode.InternalServerError,
                    ErrorResponse(
                        e.message ?: "Internal server error",
                        HttpStatusCode.InternalServerError
                    )
                )
            }
        }
        delete ("{slug}") {
            try {
                val slug = call.parameters["slug"] ?:
                    throw MissingParameterException("slug")
                val seats = try {
                    call.request.queryParameters["seats"]?.toInt()
                }
                catch (e: NumberFormatException) {
                    throw BadParameterException("seats")
                }

                BookingService.cancelBySlug(slug, seats)
                call.respond(MessageResponse("Cancellation successful"))
            }
            catch (e: BaseException) {
                call.respond(e.status, ErrorResponse(e))
            }
            catch (e: Throwable) {
                call.respond(
                    HttpStatusCode.InternalServerError,
                    ErrorResponse(
                        e.message ?: "Internal server error",
                        HttpStatusCode.InternalServerError
                    )
                )
            }
        }
        delete {
            try {
                val request = call.receive<BookingService.CancellationRequest>()
                val seats = try {
                    call.request.queryParameters["seats"]?.toInt()
                }
                catch (e: NumberFormatException) {
                    throw BadParameterException("seats")
                }
                BookingService.cancelByCode(request, seats)
                call.respond(MessageResponse("Cancellation successful"))
            }
            catch (e: BaseException) {
                call.respond(e.status, ErrorResponse(e))
            }
            catch (e: Throwable) {
                call.respond(
                    HttpStatusCode.InternalServerError,
                    ErrorResponse(
                        e.message ?: "Internal server error",
                        HttpStatusCode.InternalServerError
                    )
                )
            }
        }
    }
}

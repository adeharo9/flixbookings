package com.adeharo.routing.service

import com.adeharo.routing.Routes
import io.ktor.application.*
import io.ktor.http.content.*
import io.ktor.response.*
import io.ktor.routing.*

fun Route.docsRouting() {
    static("/static") {
        resources("docs")
    }

    route(Routes.docs) {
        get {
            call.respondRedirect("/swagger/index.html?url=/static/flixbookings.json", true)
        }
    }
}

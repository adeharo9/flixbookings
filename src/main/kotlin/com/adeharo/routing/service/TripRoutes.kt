package com.adeharo.routing.service

import com.adeharo.application.exception.BaseException
import com.adeharo.application.exception.MissingParameterException
import com.adeharo.application.service.TripService
import com.adeharo.routing.Routes
import com.adeharo.routing.response.DataResponse
import com.adeharo.routing.response.ErrorResponse
import com.adeharo.routing.response.HrefResponse
import com.adeharo.routing.response.MessageResponse
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.util.*

fun Route.tripRouting() {
    route(Routes.trip) {
        get ("{slug}") {
            try {
                val slug = call.parameters["slug"] ?:
                throw MissingParameterException("slug")
                val trip = TripService.getOneBySlug(slug)
                call.respond(DataResponse(trip))
            }
            catch (e: BaseException) {
                call.respond(e.status, ErrorResponse(e))
            }
            catch (e: Throwable) {
                call.respond(
                    HttpStatusCode.InternalServerError,
                    ErrorResponse(
                        e.message ?: "Internal server error",
                        HttpStatusCode.InternalServerError
                    )
                )
            }
        }
        get {
            try {
                val filters = call.request.queryParameters.toMap()
                val trips = TripService.getAll(filters)
                call.respond(DataResponse(trips))
            }
            catch (e: BaseException) {
                call.respond(e.status, ErrorResponse(e))
            }
            catch (e: Throwable) {
                call.respond(
                    HttpStatusCode.InternalServerError,
                    ErrorResponse(
                        e.message ?: "Internal server error",
                        HttpStatusCode.InternalServerError
                    )
                )
            }
        }
        post {
            try {
                val request = call.receive<TripService.InsertOneRequest>()
                val slug = TripService.insertOne(request)
                call.respond(HrefResponse(
                    "${Routes.trip}/$slug",
                    "Trip insertion successful"
                ))
            }
            catch (e: BaseException) {
                call.respond(e.status, ErrorResponse(e))
            }
            catch (e: Throwable) {
                call.respond(
                    HttpStatusCode.InternalServerError,
                    ErrorResponse(
                        e.message ?: "Internal server error",
                        HttpStatusCode.InternalServerError
                    )
                )
            }
        }
        delete ("{slug}") {
            try {
                val slug = call.parameters["slug"] ?:
                    throw MissingParameterException("slug")
                TripService.deleteOneBySlug(slug)
                call.respond(MessageResponse("Trip deletion successful"))
            }
            catch (e: BaseException) {
                call.respond(e.status, ErrorResponse(e))
            }
            catch (e: Throwable) {
                call.respond(
                    HttpStatusCode.InternalServerError,
                    ErrorResponse(
                        e.message ?: "Internal server error",
                        HttpStatusCode.InternalServerError
                    )
                )
            }
        }
    }
}
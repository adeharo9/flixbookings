package com.adeharo.routing.service

import com.adeharo.application.exception.BaseException
import com.adeharo.application.exception.MissingParameterException
import com.adeharo.application.service.UserService
import com.adeharo.routing.Routes
import com.adeharo.routing.response.DataResponse
import com.adeharo.routing.response.ErrorResponse
import com.adeharo.routing.response.HrefResponse
import com.adeharo.routing.response.MessageResponse
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.util.*

fun Route.userRouting() {
    route(Routes.user) {
        get ("{slug}") {
            try {
                val slug = call.parameters["slug"] ?:
                    throw MissingParameterException("slug")
                val user = UserService.getOneBySlug(slug)
                call.respond(DataResponse(user))
            }
            catch (e: BaseException) {
                call.respond(e.status, ErrorResponse(e))
            }
            catch (e: Throwable) {
                call.respond(
                    HttpStatusCode.InternalServerError,
                    ErrorResponse(
                        e.message ?: "Internal server error",
                        HttpStatusCode.InternalServerError
                    )
                )
            }
        }
        get {
            try {
                val filters = call.request.queryParameters.toMap()
                val users = UserService.getAll(filters)
                call.respond(DataResponse(users))
            }
            catch (e: BaseException) {
                call.respond(e.status, ErrorResponse(e))
            }
            catch (e: Throwable) {
                call.respond(
                    HttpStatusCode.InternalServerError,
                    ErrorResponse(
                        e.message ?: "Internal server error",
                        HttpStatusCode.InternalServerError
                    )
                )
            }
        }
        post {
            try {
                val request = call.receive<UserService.InsertOneRequest>()
                val slug = UserService.insertOne(request)
                call.respond(HrefResponse(
                    "${Routes.user}/$slug",
                    "User insertion successful"
                ))
            }
            catch (e: BaseException) {
                call.respond(e.status, ErrorResponse(e))
            }
            catch (e: Throwable) {
                call.respond(
                    HttpStatusCode.InternalServerError,
                    ErrorResponse(
                        e.message ?: "Internal server error",
                        HttpStatusCode.InternalServerError
                    )
                )
            }
        }
        delete ("{slug}") {
            try {
                val slug = call.parameters["slug"] ?:
                    throw MissingParameterException("slug")
                UserService.deleteOneBySlug(slug)
                call.respond(MessageResponse("User deletion successful"))
            }
            catch (e: BaseException) {
                call.respond(e.status, ErrorResponse(e))
            }
            catch (e: Throwable) {
                call.respond(
                    HttpStatusCode.InternalServerError,
                    ErrorResponse(
                        e.message ?: "Internal server error",
                        HttpStatusCode.InternalServerError
                    )
                )
            }
        }
    }
}
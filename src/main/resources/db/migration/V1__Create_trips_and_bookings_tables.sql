CREATE TABLE users (
    id INT NOT NULL AUTO_INCREMENT,
    slug VARCHAR(16) NOT NULL,
    email VARCHAR(255) NOT NULL,
    password TEXT NULL,
    registered BOOLEAN NOT NULL DEFAULT FALSE,
    created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    deleted_at DATETIME NULL DEFAULT NULL,
    PRIMARY KEY (id),
    UNIQUE (slug),
    UNIQUE (email),
    INDEX slug_index(slug),
    INDEX email_index(email)
);

CREATE TABLE trips (
    id INT NOT NULL AUTO_INCREMENT,
    slug VARCHAR(16) NOT NULL,
    origin TEXT NOT NULL,
    destination TEXT NOT NULL,
    departure DATETIME NOT NULL,
    available_seats INT NOT NULL,
    created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    deleted_at DATETIME NULL DEFAULT NULL,
    PRIMARY KEY (id),
    UNIQUE (slug),
    INDEX slug_index(slug)
);

CREATE TABLE bookings (
    id INT NOT NULL AUTO_INCREMENT,
    slug VARCHAR(16) NOT NULL,
    code VARCHAR(6) NOT NULL,
    user_id INT NOT NULL,
    trip_id INT NOT NULL,
    seats INT NOT NULL,
    created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    deleted_at DATETIME NULL DEFAULT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (user_id)
        REFERENCES users(id)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    FOREIGN KEY (trip_id)
        REFERENCES trips(id)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    UNIQUE (slug),
    UNIQUE (code),
    INDEX slug_index(slug),
    INDEX code_index(code)
);

package com.adeharo

import com.adeharo.routing.response.BaseResponse
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.server.testing.*
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

abstract class BaseTest {
    protected fun testContents(test: TestApplicationEngine.() -> Unit) {
        withTestApplication(Application::module) {
            test()
        }
    }

    protected fun assertResponseFormat(
        response: TestApplicationResponse,
        expectedStatus: HttpStatusCode
    ) {
        assertEquals(expectedStatus, response.status())
        assertNotNull(response.content)
        val contentTypeString = assertNotNull(
            response.headers[HttpHeaders.ContentType]
        )
        assertEquals(
            ContentType.Application.Json.withCharset(Charsets.UTF_8),
            ContentType.parse(contentTypeString)
        )
    }

    protected inline fun <reified T: BaseResponse> assertResponseContentFormat(
        content: String,
        expectedStatus: HttpStatusCode
    ): T {
        lateinit var responseContent: T

        try {
            responseContent = Json.decodeFromString(content)
        }
        catch (e: Throwable) {
            assertTrue(false, e.message)
            throw e
        }
        assertEquals(
            expectedStatus.value,
            responseContent.status.code
        )
        assertEquals(
            expectedStatus.description,
            responseContent.status.description
        )

        return responseContent
    }
}
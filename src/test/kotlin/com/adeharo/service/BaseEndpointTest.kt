package com.adeharo.service

import com.adeharo.BaseTest
import com.adeharo.routing.response.BaseResponse
import io.ktor.http.*
import io.ktor.server.testing.*

abstract class BaseEndpointTest: BaseTest() {
    protected inline fun <reified T: BaseResponse> TestApplicationEngine.getOne(
        status: HttpStatusCode = HttpStatusCode.OK,
        uri: String,
        assertions: TestApplicationEngine.(T) -> Unit = {}
    ) {
        handleRequest(HttpMethod.Get, uri).apply {
            assertResponseFormat(response, status)
            val content: T = assertResponseContentFormat(
                response.content as String,
                status
            )

            assertions(content)
        }
    }

    protected inline fun <reified T: BaseResponse> TestApplicationEngine.insertOne(
        status: HttpStatusCode = HttpStatusCode.OK,
        uri: String,
        element: String,
        assertions: TestApplicationEngine.(T) -> Unit = {}
    ) {
        with(handleRequest(HttpMethod.Post, uri) {
            addHeader(
                HttpHeaders.ContentType,
                ContentType.Application.Json.toString()
            )
            setBody(element)
        }) {
            assertResponseFormat(response, status)
            val content: T = assertResponseContentFormat(
                response.content as String,
                status
            )

            assertions(content)
        }
    }

    protected inline fun <reified T: BaseResponse> TestApplicationEngine.deleteOne(
        status: HttpStatusCode = HttpStatusCode.OK,
        uri: String,
        body: String? = null,
        assertions: TestApplicationEngine.(T) -> Unit
    ) {
        if (body == null) {
            handleRequest(HttpMethod.Delete, uri).apply {
                assertResponseFormat(response, status)
                val content: T = assertResponseContentFormat(
                    response.content as String,
                    status
                )
                assertions(content)
            }
        }
        else {
            with(handleRequest(HttpMethod.Delete, uri) {
                addHeader(
                    HttpHeaders.ContentType,
                    ContentType.Application.Json.toString()
                )
                setBody(body)
            }) {
                assertResponseFormat(response, status)
                val content: T = assertResponseContentFormat(
                    response.content as String,
                    status
                )

                assertions(content)
            }
        }
    }
}
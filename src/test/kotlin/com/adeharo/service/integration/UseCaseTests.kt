package com.adeharo.service.integration

import com.adeharo.domain.model.entity.Booking
import com.adeharo.domain.model.entity.Trip
import com.adeharo.domain.model.entity.User
import com.adeharo.domain.model.value.BookingCode
import com.adeharo.domain.model.value.URI
import com.adeharo.routing.Routes
import com.adeharo.routing.response.DataResponse
import com.adeharo.routing.response.ErrorResponse
import com.adeharo.routing.response.HrefResponse
import com.adeharo.routing.response.MessageResponse
import com.adeharo.service.BaseEndpointTest
import com.adeharo.util.randomJson
import io.ktor.http.*
import io.ktor.server.testing.*
import kotlin.test.*

class UseCaseTests: BaseEndpointTest() {
    private lateinit var user: User
    private lateinit var trip: Trip

    init {
        /** Prepare data common to all tests */
        testContents {
            lateinit var tripURI: String
            lateinit var userURI: String

            /** Insert a valid user */
            insertOne<HrefResponse>(
                uri = Routes.user,
                element = User.randomJson()
            ) {
                userURI = it.href
            }
            getOne<DataResponse<User>>(uri = userURI) {
                user = it.data
            }

            /** Insert a valid trip */
            insertOne<HrefResponse>(
                uri = Routes.trip,
                element = Trip.randomJson(availableSeats = 54)
            ) {
                tripURI = it.href
            }
            getOne<DataResponse<Trip>>(uri = tripURI) {
                trip = it.data
            }
        }
    }

    /** POSITIVE TESTS */

    @Test
    fun validBookingWithoutRegistering() {
        val bookingSeats = (1..trip.availableSeats).random()
        lateinit var uri: String

        /** Check the randomly-generated data is correct */
        assertTrue(bookingSeats > 0)
        assertTrue(trip.availableSeats >= bookingSeats)

        testContents {
            /** Generate the booking */
            insertOne<HrefResponse>(
                uri = Routes.booking,
                element = Booking.randomJson(
                    user.email,
                    trip.self.slug.toString(),
                    bookingSeats
                )
            ) {
                uri = it.href
            }

            /** Check the booking has been correctly created */
            getOne<DataResponse<Booking>>(uri = uri) {
                val booking = it.data
                assertNotNull(booking.id)
                assertEquals(URI.fromString(uri), booking.self)
                assertTrue(BookingCode.isValid(booking.code))
                assertEquals(user, booking.user)
                assertEquals(trip, booking.trip)
                assertEquals(bookingSeats, booking.seats)
                assertNotNull(booking.createdAt)
                assertNotNull(booking.updatedAt)
                assertNull(booking.deletedAt)
            }

            /** Check the trip has been correctly updated */
            getOne<DataResponse<Trip>>(uri = trip.self.toString()) {
                val prevAvailableSeats = trip.availableSeats
                trip = it.data
                assertEquals(prevAvailableSeats - bookingSeats, trip.availableSeats)
            }
        }
    }

    @Test
    fun cancelWholeBooking() {
        val bookingSeats = (1..trip.availableSeats).random()
        lateinit var booking: Booking
        lateinit var uri: String

        /** Check the randomly-generated data is correct */
        assertTrue(bookingSeats > 0)
        assertTrue(trip.availableSeats >= bookingSeats)

        testContents {
            /** Generate the booking */
            insertOne<HrefResponse>(
                uri = Routes.booking,
                element = Booking.randomJson(
                    user.email,
                    trip.self.slug.toString(),
                    bookingSeats
                )
            ) {
                uri = it.href
            }

            /** Check the booking has been correctly created */
            getOne<DataResponse<Booking>>(uri = uri) {
                booking = it.data
                assertNotNull(booking.id)
                assertEquals(URI.fromString(uri), booking.self)
                assertTrue(BookingCode.isValid(booking.code))
                assertEquals(user, booking.user)
                assertEquals(trip, booking.trip)
                assertEquals(bookingSeats, booking.seats)
                assertNotNull(booking.createdAt)
                assertNotNull(booking.updatedAt)
                assertNull(booking.deletedAt)
            }

            /** Check the trip has been correctly updated */
            getOne<DataResponse<Trip>>(uri = trip.self.toString()) {
                val prevAvailableSeats = trip.availableSeats
                trip = it.data
                assertEquals(prevAvailableSeats - booking.seats, trip.availableSeats)
            }

            /** Cancel the whole booking */
            deleteOne<MessageResponse>(
                uri = Routes.booking,
                body = """
                    {
                        "email": "${user.email}",
                        "code": "${booking.code}"
                    }
                """.trimIndent()
            ) {
                assertTrue(it.message.isNotBlank())
            }

            /** Check the booking does not exist */
            getOne<ErrorResponse>(
                HttpStatusCode.NotFound,
                "${Routes.booking}/${booking.self.slug}"
            ) {
                assertTrue(it.error.isNotBlank())
            }

            /** Check the trip has been correctly updated */
            getOne<DataResponse<Trip>>(uri = trip.self.toString()) {
                val prevAvailableSeats = trip.availableSeats
                trip = it.data
                assertEquals(prevAvailableSeats + booking.seats, trip.availableSeats)
            }
        }
    }

    @Test
    fun cancelPartialBooking() {
        val bookingSeats = (2..trip.availableSeats).random()
        val cancelSeats = (1 until bookingSeats).random()
        lateinit var booking: Booking
        lateinit var uri: String

        /** Check the randomly-generated data is correct */
        assertTrue(bookingSeats > 1)
        assertTrue(cancelSeats > 0)
        assertTrue(trip.availableSeats >= bookingSeats)
        assertTrue(bookingSeats > cancelSeats)

        testContents {
            /** Generate the booking */
            insertOne<HrefResponse>(
                uri = Routes.booking,
                element = Booking.randomJson(
                    user.email,
                    trip.self.slug.toString(),
                    bookingSeats
                )
            ) {
                uri = it.href
            }

            /** Check the booking has been correctly created */
            getOne<DataResponse<Booking>>(uri = uri) {
                booking = it.data
                assertNotNull(booking.id)
                assertEquals(URI.fromString(uri), booking.self)
                assertTrue(BookingCode.isValid(booking.code))
                assertEquals(user, booking.user)
                assertEquals(trip, booking.trip)
                assertEquals(bookingSeats, booking.seats)
                assertNotNull(booking.createdAt)
                assertNotNull(booking.updatedAt)
                assertNull(booking.deletedAt)
            }

            /** Check the trip has been correctly updated */
            getOne<DataResponse<Trip>>(uri = trip.self.toString()) {
                val prevAvailableSeats = trip.availableSeats
                trip = it.data
                assertEquals(prevAvailableSeats - booking.seats, trip.availableSeats)
            }

            /** Cancel the whole booking */
            deleteOne<MessageResponse>(
                uri = "${Routes.booking}?seats=$cancelSeats",
                body = """
                    {
                        "email": "${user.email}",
                        "code": "${booking.code}"
                    }
                """.trimIndent()
            ) {
                assertTrue(it.message.isNotBlank())
            }

            /** Check the booking still exists */
            getOne<DataResponse<Booking>>(uri = "${Routes.booking}/${booking.self.slug}") {
                booking = it.data
                assertEquals(bookingSeats - cancelSeats, booking.seats)
            }

            /** Check the trip has been correctly updated */
            getOne<DataResponse<Trip>>(uri = trip.self.toString()) {
                val prevAvailableSeats = trip.availableSeats
                trip = it.data
                assertEquals(prevAvailableSeats + cancelSeats, trip.availableSeats)
            }
        }
    }

    @Test
    fun getAllBookingsFromTrip() {
        val expectedStatus = HttpStatusCode.OK
        val n = 3

        testContents {
            for (i in 1..n) {
                insertOne<HrefResponse>(
                    uri = Routes.booking,
                    element = Booking.randomJson(
                        user.email,
                        trip.self.slug.toString(),
                        2
                    )
                )
            }

            handleRequest(
                HttpMethod.Get,
                "${Routes.booking}?trip=${trip.self.slug}"
            ).apply {
                assertResponseFormat(response, expectedStatus)
                val content: DataResponse<List<Booking>> =
                    assertResponseContentFormat(
                        response.content as String,
                        expectedStatus
                    )
                assertEquals(content.data.size, n)
            }
        }
    }
}
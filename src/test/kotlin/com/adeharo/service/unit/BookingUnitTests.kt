package com.adeharo.service.unit

import com.adeharo.domain.model.entity.Booking
import com.adeharo.domain.model.entity.Trip
import com.adeharo.domain.model.entity.User
import com.adeharo.domain.model.value.BookingCode
import com.adeharo.domain.model.value.Email
import com.adeharo.domain.model.value.Slug
import com.adeharo.domain.model.value.URI
import com.adeharo.routing.Routes
import com.adeharo.routing.response.DataResponse
import com.adeharo.routing.response.ErrorResponse
import com.adeharo.routing.response.HrefResponse
import com.adeharo.routing.response.MessageResponse
import com.adeharo.service.BaseEndpointTest
import com.adeharo.util.random
import com.adeharo.util.randomJson
import io.ktor.http.*
import io.ktor.server.testing.*
import kotlin.test.*

class BookingUnitTests: BaseEndpointTest() {
    private val endpoint: String = Routes.booking
    private lateinit var user: User
    private lateinit var trip: Trip

    init {
        /** Prepare data common to all tests */
        testContents {
            lateinit var tripURI: String
            lateinit var userURI: String

            /** Insert a valid user */
            insertOne<HrefResponse>(
                uri = Routes.user,
                element = User.randomJson()
            ) {
                userURI = it.href
            }
            getOne<DataResponse<User>>(uri = userURI) {
                user = it.data
            }

            /** Insert a valid trip */
            insertOne<HrefResponse>(
                uri = Routes.trip,
                element = Trip.randomJson(availableSeats = 54)
            ) {
                tripURI = it.href
            }
            getOne<DataResponse<Trip>>(uri = tripURI) {
                trip = it.data
            }
        }
    }

    /** POSITIVE TESTS */

    @Test
    fun getAllBookings() {
        val expectedStatus = HttpStatusCode.OK
        val n = 2

        testContents {
            for (i in 1..n) {
                insertOne<HrefResponse>(
                    uri = endpoint,
                    element = Booking.randomJson(
                        user.email,
                        trip.self.slug.toString(),
                        2
                    )
                )
            }

            handleRequest(HttpMethod.Get, endpoint).apply {
                assertResponseFormat(response, expectedStatus)
                val content: DataResponse<List<Booking>> =
                    assertResponseContentFormat(
                        response.content as String,
                        expectedStatus
                    )
                assertTrue(content.data.size >= n)
            }
        }
    }

    @Test
    fun getExistingBooking() {
        lateinit var uri: String

        testContents {
            val bookingSeats = (1..trip.availableSeats).random()
            insertOne<HrefResponse>(
                uri = endpoint,
                element = Booking.randomJson(
                    user.email,
                    trip.self.slug.toString(),
                    bookingSeats
                )
            ) {
                uri = it.href
            }
            getOne<DataResponse<Booking>>(uri = uri) {
                val booking = it.data
                assertNotNull(booking.id)
                assertEquals(URI.fromString(uri), booking.self)
                assertTrue(BookingCode.isValid(booking.code))
                assertEquals(user, booking.user)
                assertEquals(trip, booking.trip)
                assertEquals(bookingSeats, booking.seats)
                assertNotNull(booking.createdAt)
                assertNotNull(booking.updatedAt)
                assertNull(booking.deletedAt)
            }
        }
    }

    @Test
    fun insertValidBooking() {
        testContents {
            insertOne<HrefResponse>(
                uri = endpoint,
                element = Booking.randomJson(
                     user.email,
                     trip.self.slug.toString(),
                     (1..trip.availableSeats).random()
                )
            ) {
                assertTrue(it.href.isNotBlank())
            }
        }
    }

    @Test
    fun deleteExistingBooking() {
        lateinit var uri: String

        testContents {
            insertOne<HrefResponse>(
                uri = endpoint,
                element = Booking.randomJson(
                    user.email,
                    trip.self.slug.toString(),
                    (1..trip.availableSeats).random()
                )
            ) {
                uri = it.href
            }
            deleteOne<MessageResponse>(uri = uri) {
                assertTrue(it.message.isNotBlank())
            }
            getOne<ErrorResponse>(HttpStatusCode.NotFound, uri) {
                assertTrue(it.error.isNotBlank())
            }
        }
    }

    /** NEGATIVE TESTS */

    @Test
    fun getNonExistingBooking() {
        val nonExistingSlug = Slug.random.toString()

        assertTrue(Slug.isValid(nonExistingSlug))

        testContents {
            getOne<ErrorResponse>(
                HttpStatusCode.NotFound,
                endpoint.plus("/$nonExistingSlug")
            ) {
                assertTrue(it.error.isNotBlank())
            }
        }
    }

    @Test
    fun getBookingInvalidSlugInvalidChar() {
        val invalidSlug = "00000000-0000000"

        assertFalse(Slug.isValid(invalidSlug))

        testContents {
            getOne<ErrorResponse>(
                HttpStatusCode.BadRequest,
                endpoint.plus("/$invalidSlug")
            ) {
                assertTrue(it.error.isNotBlank())
            }
        }
    }

    @Test
    fun getBookingInvalidSlugShort() {
        val invalidSlug = "0123456789"

        assertFalse(Slug.isValid(invalidSlug))

        testContents {
            getOne<ErrorResponse>(
                HttpStatusCode.BadRequest,
                endpoint.plus("/$invalidSlug")
            ) {
                assertTrue(it.error.isNotBlank())
            }
        }
    }

    @Test
    fun insertBookingNonExistingUser() {
        testContents {
            insertOne<ErrorResponse>(
                HttpStatusCode.NotFound,
                endpoint,
                Booking.randomJson(
                    Email.random().toString(),
                    trip.self.slug.toString(),
                    (1..trip.availableSeats).random()
                )
            ) {
                assertTrue(it.error.isNotBlank())
            }
        }
    }

    @Test
    fun insertBookingNonExistingTrip() {
        testContents {
            insertOne<ErrorResponse>(
                HttpStatusCode.NotFound,
                endpoint,
                Booking.randomJson(
                    user.email,
                    Slug.random.toString(),
                    (1..trip.availableSeats).random()
                )
            ) {
                assertTrue(it.error.isNotBlank())
            }
        }
    }

    @Test
    fun insertBookingOverbook() {
        testContents {
            insertOne<ErrorResponse>(
                HttpStatusCode.Forbidden,
                endpoint,
                Booking.randomJson(
                    user.email,
                    trip.self.slug.toString(),
                    trip.availableSeats + 1
                )
            ) {
                assertTrue(it.error.isNotBlank())
            }
        }
    }

    @Test
    fun insertBookingUnderbook() {
        testContents {
            insertOne<ErrorResponse>(
                HttpStatusCode.BadRequest,
                endpoint,
                Booking.randomJson(
                    user.email,
                    trip.self.slug.toString(),
                    0
                )
            ) {
                assertTrue(it.error.isNotBlank())
            }
        }
    }

    @Test
    fun deleteNonExistingBooking() {
        testContents {
            deleteOne<ErrorResponse>(
                HttpStatusCode.NotFound,
                endpoint.plus("/${Slug.random}")
            ) {
                assertTrue(it.error.isNotBlank())
            }
        }
    }

    @Test
    fun deleteBookingOvercancel() {
        val bookingSeats = (1..trip.availableSeats).random()
        lateinit var uri: String

        assertTrue(bookingSeats > 0)

        testContents {
            insertOne<HrefResponse>(
                uri = endpoint,
                element = Booking.randomJson(
                    user.email,
                    trip.self.slug.toString(),
                    bookingSeats
                )
            ) {
                uri = it.href
            }
            deleteOne<ErrorResponse>(
                HttpStatusCode.BadRequest,
                "$uri?seats=${bookingSeats + 1}"
            ) {
                assertTrue(it.error.isNotBlank())
            }
        }
    }

    @Test
    fun deleteBookingUndercancel() {
        lateinit var uri: String

        testContents {
            insertOne<HrefResponse>(
                uri = endpoint,
                element = Booking.randomJson(
                    user.email,
                    trip.self.slug.toString(),
                    (1..trip.availableSeats).random()
                )
            ) {
                uri = it.href
            }
            deleteOne<ErrorResponse>(
                HttpStatusCode.BadRequest,
                "$uri?seats=0"
            ) {
                assertTrue(it.error.isNotBlank())
            }
        }
    }
}
package com.adeharo.service.unit

import com.adeharo.domain.model.entity.Trip
import com.adeharo.domain.model.value.Slug
import com.adeharo.domain.model.value.URI
import com.adeharo.routing.Routes
import com.adeharo.routing.response.DataResponse
import com.adeharo.routing.response.ErrorResponse
import com.adeharo.routing.response.HrefResponse
import com.adeharo.routing.response.MessageResponse
import com.adeharo.service.BaseEndpointTest
import com.adeharo.util.random
import com.adeharo.util.randomISO8604
import com.adeharo.util.randomJson
import io.ktor.http.*
import io.ktor.server.testing.*
import java.time.ZoneId
import java.time.ZonedDateTime
import kotlin.test.*

class TripUnitTests: BaseEndpointTest() {
    val endpoint: String = Routes.trip

    /** POSITIVE TESTS */

    @Test
    fun getAllTrips() {
        val expectedStatus = HttpStatusCode.OK
        val n = 2

        testContents {
            for (i in 1..n) {
                insertOne<HrefResponse>(
                    uri = endpoint,
                    element = Trip.randomJson()
                )
            }

            handleRequest(HttpMethod.Get, endpoint).apply {
                assertResponseFormat(response, expectedStatus)
                val content: DataResponse<List<Trip>> =
                    assertResponseContentFormat(
                        response.content as String,
                        expectedStatus
                    )
                assertTrue(content.data.size >= n)
            }
        }
    }

    @Test
    fun getExistingTrip() {
        val origin = String.random()
        val destination = String.random()
        val departure = String.randomISO8604()
        val availableSeats = (1..81).random()
        lateinit var uri: String

        assertTrue(availableSeats > 0)

        testContents {
            insertOne<HrefResponse>(
                uri = endpoint,
                element = Trip.randomJson(origin, destination, departure, availableSeats)
            ) {
                uri = it.href
            }
            getOne<DataResponse<Trip>>(uri = uri) {
                val trip = it.data
                assertNotNull(trip.id)
                assertEquals(URI.fromString(uri), trip.self)
                assertEquals(origin, trip.origin)
                assertEquals(destination, trip.destination)
                val departureUTC = ZonedDateTime
                    .parse(departure)
                    .withZoneSameInstant(ZoneId.of("Z"))
                assertEquals(departureUTC, trip.departure)
                assertEquals(availableSeats, trip.availableSeats)
                assertNotNull(trip.createdAt)
                assertNotNull(trip.updatedAt)
                assertNull(trip.deletedAt)
            }
        }
    }

    @Test
    fun insertValidTrip() {
        testContents {
            insertOne<HrefResponse>(
                uri = endpoint,
                element = Trip.randomJson()
            ) {
                assertTrue(it.href.isNotBlank())
            }
        }
    }

    @Test
    fun deleteExistingTrip() {
        lateinit var uri: String

        testContents {
            insertOne<HrefResponse>(
                uri = endpoint,
                element = Trip.randomJson()
            ) {
                uri = it.href
            }
            deleteOne<MessageResponse>(uri = uri) {
                assertTrue(it.message.isNotBlank())
            }
            getOne<ErrorResponse>(HttpStatusCode.NotFound, uri) {
                assertTrue(it.error.isNotBlank())
            }
        }
    }

    /** NEGATIVE TESTS */

    @Test
    fun getNonExistingTrip() {
        val nonExistingSlug = "${Slug.random}"

        assertTrue(Slug.isValid(nonExistingSlug))

        testContents {
            getOne<ErrorResponse>(
                HttpStatusCode.NotFound,
                endpoint.plus("/$nonExistingSlug")
            ) {
                assertTrue(it.error.isNotBlank())
            }
        }
    }

    @Test
    fun getTripInvalidSlugInvalidChar() {
        val invalidSlug = "00000000-0000000"

        assertFalse(Slug.isValid(invalidSlug))

        testContents {
            getOne<ErrorResponse>(
                HttpStatusCode.BadRequest,
                endpoint.plus("/$invalidSlug")
            ) {
                assertTrue(it.error.isNotBlank())
            }
        }
    }

    @Test
    fun getTripInvalidSlugShort() {
        val invalidSlug = "0123456789"

        assertFalse(Slug.isValid(invalidSlug))

        testContents {
            getOne<ErrorResponse>(
                HttpStatusCode.BadRequest,
                endpoint.plus("/$invalidSlug")
            ) {
                assertTrue(it.error.isNotBlank())
            }
        }
    }

    @Test
    fun insertTripWithInvalidAvailableSeats() {
        val invalidAvailableSeats = (-81..0).random()

        assertTrue(invalidAvailableSeats <= 0)

        testContents {
            insertOne<ErrorResponse>(
                HttpStatusCode.BadRequest,
                endpoint,
                Trip.randomJson(availableSeats = invalidAvailableSeats)
            ) {
                assertTrue(it.error.isNotBlank())
            }
        }
    }

    @Test
    fun deleteNonExistingTrip() {
        val nonExistingSlug = Slug.random.toString()

        assertTrue(Slug.isValid(nonExistingSlug))

        testContents {
            deleteOne<ErrorResponse>(
                HttpStatusCode.NotFound,
                endpoint.plus("/$nonExistingSlug")
            ) {
                assertTrue(it.error.isNotBlank())
            }
        }
    }
}
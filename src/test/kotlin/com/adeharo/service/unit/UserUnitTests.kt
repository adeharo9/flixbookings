package com.adeharo.service.unit

import com.adeharo.domain.model.entity.User
import com.adeharo.domain.model.value.Email
import com.adeharo.domain.model.value.Slug
import com.adeharo.domain.model.value.URI
import com.adeharo.routing.Routes
import com.adeharo.routing.response.DataResponse
import com.adeharo.routing.response.ErrorResponse
import com.adeharo.routing.response.HrefResponse
import com.adeharo.routing.response.MessageResponse
import com.adeharo.service.BaseEndpointTest
import com.adeharo.util.random
import com.adeharo.util.randomJson
import io.ktor.http.*
import io.ktor.server.testing.*
import kotlin.test.*

class UserUnitTests: BaseEndpointTest() {
    private val endpoint: String = Routes.user

    /** POSITIVE TESTS */

    @Test
    fun getAllUsers() {
        val expectedStatus = HttpStatusCode.OK
        val n = 2

        testContents {
            for (i in 1..n) {
                insertOne<HrefResponse>(
                    uri = endpoint,
                    element = User.randomJson()
                )
            }

            handleRequest(HttpMethod.Get, endpoint).apply {
                assertResponseFormat(response, HttpStatusCode.OK)
                val content: DataResponse<List<User>> =
                    assertResponseContentFormat(
                        response.content as String,
                        expectedStatus
                    )
                assertTrue(content.data.size >= n)
            }
        }
    }

    @Test
    fun getExistingUser() {
        val email = Email.random().toString()
        lateinit var uri: String

        assertTrue(Email.isValid(email))

        testContents {
            insertOne<HrefResponse>(
                uri = endpoint,
                element = User.randomJson(email)
            ) {
                uri = it.href
            }
            getOne<DataResponse<User>>(uri = uri) {
                val user = it.data
                assertNotNull(user.id)
                assertEquals(URI.fromString(uri), user.self)
                assertEquals(email, user.email)
                assertNull(user.password)
                assertFalse(user.registered)
                assertNotNull(user.createdAt)
                assertNotNull(user.updatedAt)
                assertNull(user.deletedAt)
            }
        }
    }

    @Test
    fun insertValidUser() {
        testContents {
            insertOne<HrefResponse>(
                uri = endpoint,
                element = User.randomJson()
            ) {
                assertTrue(it.href.isNotBlank())
            }
        }
    }

    @Test
    fun deleteExistingUser() {
        lateinit var uri: String

        testContents {
            insertOne<HrefResponse>(
                uri = endpoint,
                element = User.randomJson()
            ) {
                uri = it.href
            }
            deleteOne<MessageResponse>(uri = uri) {
                assertTrue(it.message.isNotBlank())
            }
            getOne<ErrorResponse>(HttpStatusCode.NotFound, uri) {
                assertTrue(it.error.isNotBlank())
            }
        }
    }

    /** NEGATIVE TESTS */

    @Test
    fun getNonExistingUser() {
        val nonExistingSlug = Slug.random.toString()

        assertTrue(Slug.isValid(nonExistingSlug))

        testContents {
            getOne<ErrorResponse>(
                HttpStatusCode.NotFound,
                endpoint.plus("/$nonExistingSlug")
            ) {
                assertTrue(it.error.isNotBlank())
            }
        }
    }

    @Test
    fun getUserInvalidSlugInvalidChar() {
        val invalidSlug = "00000000-0000000"

        assertFalse(Slug.isValid(invalidSlug))

        testContents {
            getOne<ErrorResponse>(
                HttpStatusCode.BadRequest,
                endpoint.plus("/$invalidSlug")
            ) {
                assertTrue(it.error.isNotBlank())
            }
        }
    }

    @Test
    fun getUserInvalidSlugShort() {
        val invalidSlug = "0123456789"

        assertFalse(Slug.isValid(invalidSlug))

        testContents {
            getOne<ErrorResponse>(
                HttpStatusCode.BadRequest,
                endpoint.plus("/$invalidSlug")
            ) {
                assertTrue(it.error.isNotBlank())
            }
        }
    }

    @Test
    fun insertUserWithInvalidEmail0() {
        val invalidEmail = "?thisisinvalid0@flixbus.com"

        assertFalse(Email.isValid(invalidEmail))

        testContents {
            insertOne<ErrorResponse>(
                HttpStatusCode.BadRequest,
                endpoint,
                User.randomJson(invalidEmail)
            ) {
                assertTrue(it.error.isNotBlank())
            }
        }
    }

    @Test
    fun insertUserWithInvalidEmail1() {
        val invalidEmail = "thisisinvalid1?flixbus.com"

        assertFalse(Email.isValid(invalidEmail))

        testContents {
            insertOne<ErrorResponse>(
                HttpStatusCode.BadRequest,
                endpoint,
                User.randomJson(invalidEmail)
            ) {
                assertTrue(it.error.isNotBlank())
            }
        }
    }

    @Test
    fun insertUserWithInvalidEmail2() {
        val invalidEmail = "thisisinvalid2@flixbuscom"

        assertFalse(Email.isValid(invalidEmail))

        testContents {
            insertOne<ErrorResponse>(
                HttpStatusCode.BadRequest,
                endpoint,
                User.randomJson(invalidEmail)
            ) {
                assertTrue(it.error.isNotBlank())
            }
        }
    }

    @Test
    fun deleteNonExistingUser() {
        val nonExistingSlug = Slug.random.toString()

        assertTrue(Slug.isValid(nonExistingSlug))

        testContents {
            deleteOne<ErrorResponse>(
                HttpStatusCode.NotFound,
                endpoint.plus("/$nonExistingSlug")
            ) {
                assertTrue(it.error.isNotBlank())
            }
        }
    }
}
package com.adeharo.util

import com.adeharo.domain.model.entity.Booking
import com.adeharo.domain.model.entity.Trip
import com.adeharo.domain.model.entity.User
import com.adeharo.domain.model.value.Email
import java.util.*
import kotlin.streams.asSequence

fun String.Companion.random(min: Int = 1, max: Int = 32): String {
    val l: Int = (min..max).random()
    val source = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    return Random().ints(l.toLong(), 0, source.length)
        .asSequence()
        .map(source::get)
        .joinToString("")
}

fun String.Companion.randomISO8604(): String {
    val year = (1970..9999).random().toString().padStart(4, '0')
    val month = (1..12).random().toString().padStart(2, '0')
    val day = (1..28).random().toString().padStart(2, '0')
    val hour = (0..23).random().toString().padStart(2, '0')
    val minute = (0..59).random().toString().padStart(2, '0')
    val second = (0..59).random().toString().padStart(2, '0')
    val tzSign = listOf("+", "-").random()
    val tzHour = (0..12).random().toString().padStart(2, '0')
    val tzMinute = (0..59).random().toString().padStart(2, '0')

    return "$year-$month-${day}T$hour:$minute:$second.0000$tzSign$tzHour:$tzMinute"
}

fun Email.Companion.random(): Email {
    val source = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
    val first = Random().ints(1, 0, source.length)
        .asSequence()
        .map(source::get)
        .joinToString("")
    return fromString("$first${String.random(1, 16)}@${String.random(3, 10)}.${String.random(2, 3)}")
}

fun User.Companion.randomJson(email: String? = null): String {
    val ema = email ?: Email.random().toString()

    return """
            {
                "email": "$ema"
            }
        """.trimIndent()
}

fun Trip.Companion.randomJson(
    origin: String? = null,
    destination: String? = null,
    departure: String? = null,
    availableSeats: Int? = null
): String {
    val ori = origin?: String.random()
    val des = destination?: String.random()
    val dep = departure?: String.randomISO8604()
    val ava = availableSeats ?: (1..81).random()

    return """
            {
                "origin": "$ori",
                "destination": "$des",
                "departure": "$dep",
                "available_seats": $ava
            }
        """.trimIndent()
}

fun Booking.Companion.randomJson(
    email: String,
    tripSlug: String,
    seats: Int
): String {
    return """
        {
            "email": "$email",
            "trip": "$tripSlug",
            "seats": $seats
        }
    """.trimIndent()
}